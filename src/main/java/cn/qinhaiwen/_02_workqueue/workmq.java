package cn.qinhaiwen._02_workqueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class workmq {
    public static final String NAME_WORK_QUEUE="NAME_WORK_QUEUE";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(NAME_WORK_QUEUE, true, false, false, null);
        String message ="这是条信息";

        channel.basicPublish("", NAME_WORK_QUEUE, null, message.getBytes());

    }
}
