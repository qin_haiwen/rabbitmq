package cn.qinhaiwen._02_workqueue;

import cn.qinhaiwen._01hello.rqmq;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class consummer {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.basicQos(1);
        DefaultConsumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
                try {
                    Thread.sleep(3);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("交换机："+envelope.getExchange());
                System.out.println("路由："+envelope.getRoutingKey());
                System.out.println("consumerTag:"+consumerTag);
                System.out.println("消息id:"+envelope.getDeliveryTag());

                channel.basicAck(envelope.getDeliveryTag(), false);

             }
            };
            channel.basicConsume(rqmq.NAME_HELL0, consumer);

    }
}
