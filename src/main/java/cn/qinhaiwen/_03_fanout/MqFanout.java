package cn.qinhaiwen._03_fanout;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MqFanout {
    public static final String NAME_FANOUT_PRO="NAME_FANOUT_PRO";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(NAME_FANOUT_PRO, BuiltinExchangeType.FANOUT);
        String message="哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈";
        channel.basicPublish(NAME_FANOUT_PRO, "", null, message.getBytes());
        System.out.println("发送成功");

    }
}
