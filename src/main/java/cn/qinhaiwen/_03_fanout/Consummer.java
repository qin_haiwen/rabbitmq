package cn.qinhaiwen._03_fanout;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consummer {
    public static final String QUEUQ_NAME="QUEUQ_NAME";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUQ_NAME, true, false, false, null);
        channel.queueBind(QUEUQ_NAME, MqFanout.NAME_FANOUT_PRO, "");

        DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
                System.out.println("交换机："+envelope.getExchange());
                System.out.println("路由："+envelope.getRoutingKey());
                System.out.println("consumerTag:"+consumerTag);
                System.out.println("消息id:"+envelope.getDeliveryTag());

                // 手动执行
                channel.basicAck(envelope.getDeliveryTag(), false);

            }
        };


        channel.basicConsume(QUEUQ_NAME, false, consumer);

    }
}
