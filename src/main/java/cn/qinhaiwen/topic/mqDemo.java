package cn.qinhaiwen.topic;


import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class mqDemo {
    public static final String EXANGE_TOPIC_PRODECT="EXANGE_TOPIC_PRODECT";
   public static  void main(String[] args) throws IOException, TimeoutException {

       ConnectionFactory factory = new ConnectionFactory();
       Connection connection = factory.newConnection();

       Channel channel = connection.createChannel();
       // 加true数据持久化
      channel.exchangeDeclare(EXANGE_TOPIC_PRODECT,BuiltinExchangeType.TOPIC ,true);
      // channel.queueDeclare(WORK_QUQUES, false, false, false, null);
      String message = "哈哈哈哈哈";
      // 加上MessageProperties数据持久化
       channel.basicPublish(EXANGE_TOPIC_PRODECT, "hhhh", MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
       System.out.println("发送成功");
   }
}
