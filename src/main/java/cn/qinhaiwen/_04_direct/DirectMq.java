package cn.qinhaiwen._04_direct;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class DirectMq {
    public static final String NAME_DIRECT_QUQUES="NAME_DIRECT_QUQUES";
    public static  void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();

        channel.exchangeDeclare(NAME_DIRECT_QUQUES, BuiltinExchangeType.DIRECT);
        String message = "XIXIXIXIXIXIXIX";
        channel.basicPublish(NAME_DIRECT_QUQUES,"abcd" , null, message.getBytes());
        System.out.println("发送成功");
    }
}
