package cn.qinhaiwen.mq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consummer1 {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.basicQos(1);

        DefaultConsumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // 交换机
                String exchange = envelope.getExchange();
                //路由
                String routing = envelope.getRoutingKey();
                // 消息的id
                long contentType = envelope.getDeliveryTag();
                // 消息
                System.out.println("获取信息"+new String(body));
                Thread thread = new Thread();
                try {
                    thread.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                channel.basicAck(contentType, false);
            }
        };
        channel.basicConsume(mqDemo.WORK_QUQUES, false, consumer );

    }
}
