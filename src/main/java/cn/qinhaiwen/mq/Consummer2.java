package cn.qinhaiwen.mq;


import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consummer2 {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.basicQos(1);

        DefaultConsumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
               // 交换机
               // String exchange = envelope.getExchange();
                System.out.println("交换机"+envelope.getExchange());
                //路由
                System.out.println("路由"+ envelope.getRoutingKey());
                System.out.println(consumerTag);
                //
                System.out.println("消息的id"+envelope.getDeliveryTag());

                // 消息
              //  String message = new String(body,"utf8");
                System.out.println("获取信息"+new String(body));

                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };

        channel.basicConsume(mqDemo.WORK_QUQUES, false, consumer );

    }
}
