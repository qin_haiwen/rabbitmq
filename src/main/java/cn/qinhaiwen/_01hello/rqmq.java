package cn.qinhaiwen._01hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class rqmq {
    public static final String NAME_HELL0="HELLOWORLD";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(NAME_HELL0, true, false, false, null);
        String message ="这是条信息";

        channel.basicPublish("", NAME_HELL0, null, message.getBytes());

    }
}
