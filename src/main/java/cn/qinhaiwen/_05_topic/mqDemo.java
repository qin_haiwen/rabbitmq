package cn.qinhaiwen._05_topic;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class mqDemo {
    public static final String NAME_YPOIC_PRO="NAME_YPOIC_PRO";
   public static  void main(String[] args) throws IOException, TimeoutException {

       ConnectionFactory factory = new ConnectionFactory();
       Connection connection = factory.newConnection();

       Channel channel = connection.createChannel();
       channel.exchangeDeclare(NAME_YPOIC_PRO, BuiltinExchangeType.DIRECT);
      // channel.queueDeclare(WORK_QUQUES, false, false, false, null);
      String message = "哈哈哈哈哈";
       channel.basicPublish(NAME_YPOIC_PRO, "hhhh", null, message.getBytes());
       channel.basicPublish(NAME_YPOIC_PRO, "error", null, message.getBytes());
       System.out.println("发送成功");
   }
}
