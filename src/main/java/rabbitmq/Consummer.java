package rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consummer {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        DefaultConsumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
               // 交换机
                String exchange = envelope.getExchange();
                //路由
                String routing = envelope.getRoutingKey();
                // 消息的id
                long contentType = envelope.getDeliveryTag();
                // 消息
                String message = new String(body,"utf8");

                channel.basicAck(contentType, false);

            }
        };




        channel.basicConsume(mqDemo.QUEEUNAME, false, consumer );

    }
}
