package rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class mqDemo {
    public static final String QUEEUNAME="HELLOWORD";
   public static  void main(String[] args) throws IOException, TimeoutException {

       ConnectionFactory factory = new ConnectionFactory();
       Connection connection = factory.newConnection();

       Channel channel = connection.createChannel();

       channel.queueDeclare(QUEEUNAME, false, false, false, null);
      String message = "哈哈哈哈哈";
       channel.basicPublish("", QUEEUNAME, null, message.getBytes());
       System.out.println("发送成功");
   }
}
